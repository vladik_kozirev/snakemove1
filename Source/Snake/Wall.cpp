// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			FVector MovementVector(0,0,0);
			switch (Snake->LastMoveDirection)
			{
			case EMovementDirection::UP:
				MovementVector.X -= 1970;
				break;
			case EMovementDirection::DOWN:
				MovementVector.X += 1970;
				break;
			case EMovementDirection::LEFT:
				MovementVector.Y -= 1970;
				break;
			case EMovementDirection::RIGHT:
				MovementVector.Y += 1970;
				break;
			}
			Snake->SnakeElements[0]->AddActorWorldOffset(MovementVector);
		}
	}
}

