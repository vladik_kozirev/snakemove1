// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "TeleportWall.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API ATeleportWall : public ATriggerBox
{
	GENERATED_BODY()
	
};
