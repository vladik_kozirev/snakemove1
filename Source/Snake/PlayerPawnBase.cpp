// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/World.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Let.h"
#include"Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));

	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(-90, 0, 0));

	CreateSnakeActor();

	RandomSpawnFood();

	RandomSpawnLet();

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	
}

void APlayerPawnBase::RandomSpawnFood()
{
		FVector RandomVector;
		RandomVector.X = FMath::RandRange(-910.f, 910.f);
		RandomVector.Y = FMath::RandRange(-910.f, 910.f);
		RandomVector.Z = 10.f;
		FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(RandomVector));
}

void APlayerPawnBase::RandomSpawnLet()
{
	FVector RandomVector;
	RandomVector.X = FMath::RandRange(-910.f, 910.f);
	RandomVector.Y = FMath::RandRange(-910.f, 910.f);
	RandomVector.Z = 10.f;
	LetActor = GetWorld()->SpawnActor<ALet>(LetClass, FTransform(RandomVector));
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor)) 
	{
		if (value > 0 &&SnakeActor->LastMoveDirection!=EMovementDirection::DOWN) 
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 &&SnakeActor->LastMoveDirection!=EMovementDirection::UP) 
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 &&SnakeActor->LastMoveDirection!=EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 &&SnakeActor->LastMoveDirection!=EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}



