// Fill out your copyright notice in the Description page of Project Settings.


#include "Let.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ALet::ALet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	LetMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	LetMeshComponent-> SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	LetMeshComponent->SetCollisionResponseToChannels(ECR_Block);

}

// Called when the game starts or when spawned
void ALet::BeginPlay()
{
	Super::BeginPlay();
	
	RandLet();
}

// Called every frame
void ALet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALet::RandLet()
{

}

void ALet::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake)) 
	{
			Snake->Destroy();
	}
}

