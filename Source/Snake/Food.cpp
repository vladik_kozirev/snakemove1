// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshFood = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshFoodComponent"));
	MeshFood->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshFood->SetCollisionResponseToChannels(ECR_Block);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor,bool bIsHead)
{
	if (bIsHead) 
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) 
		{
			Snake->AddSnakeElement();
			Snake->MovementSpeed *= 0.95;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			int8 Spawn = true;
			if(Spawn) 
			{
				FVector RandomVector;
				RandomVector.X = FMath::RandRange(-910.f, 910.f);
				RandomVector.Y = FMath::RandRange(-910.f, 910.f);
				RandomVector.Z = 10.f;
				FoodActors = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(RandomVector));
				Spawn = false;
			}
			this->Destroy();
		}
	}
}
