// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Let.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKE_API ALet : public AActor,public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALet();
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* LetMeshComponent;
	UPROPERTY(EditDefaultsOnly)
		ALet* Let;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ALet>LetClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void RandLet();

	virtual void Interact(AActor* Interactor, bool bIsHead)override;
};
